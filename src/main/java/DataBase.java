import java.sql.*;

public class DataBase {
    //  Database credentials
    static final String DB_URL = "jdbc:postgresql://0.0.0.0:15432/postgres";
    static final String USER = "postgres";
    static final String PASS = "postgres";

    public static void main(String[] argv) {

        System.out.println("Testing connection to PostgreSQL JDBC");

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
            return;
        }

        System.out.println("PostgreSQL JDBC Driver successfully connected");
        Connection connection = null;
        Statement stmt = null;
        try {
            try {
                connection = DriverManager
                        .getConnection(DB_URL, USER, PASS);

            } catch (SQLException e) {
                System.out.println("Connection Failed");
                e.printStackTrace();
                return;
            }

            if (connection != null) {
                System.out.println("You successfully connected to database now");
            } else {
                System.out.println("Failed to make connection to database");
            }

            //STEP 4: Execute a query and Extract data from result set
            System.out.println("Creating statement...");
            stmt = connection.createStatement();

            String firstName = null;
            String sql = "SELECT first FROM public.user";

            ResultSet rs = stmt.executeQuery(sql);
            {
                rs.next();
                //count = rs.getInt(1);
                firstName = rs.getString(1);
            }

            System.out.println("First name is: " + firstName + ".");

            //STEP 5: Clean-up environment
            rs.close();
            stmt.close();
            connection.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        System.out.println("Goodbye!");
    }
}
