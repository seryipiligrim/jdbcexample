package Helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class RemoverDB extends ConnectionData{
    public void removeRecord(int id) {
        final String REMOVE_USER = "DELETE FROM public.user WHERE id = ?;";

        // Step 1: Establishing a Connection
        try (Connection connection = DriverManager
                .getConnection(getUrl(), getUser(), getPass());

             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(REMOVE_USER);) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            ErrorHendler.printSQLException(e);
        }
        // Step 4: try-with-resource statement will auto close the connection.
    }
}