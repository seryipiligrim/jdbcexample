package Helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ReaderDB extends ConnectionData{
    public void getOneRecord(int id) {
        final String SELECT_USER = "SELECT id,first,second FROM public.user WHERE id =?";

        // Step 1: Establishing a Connection
        try (Connection connection = DriverManager
                .getConnection(getUrl(), getUser(), getPass());

             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER);) {
            preparedStatement.setInt(1, id);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            ResultSet rs = preparedStatement.executeQuery();

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                int identifier = rs.getInt("id");
                String firstName = rs.getString("first");
                String lastName = rs.getString("second");
                System.out.println(identifier + " - " + firstName + " " + lastName);
            }
        } catch (SQLException e) {
            ErrorHendler.printSQLException(e);
        }
        // Step 4: try-with-resource statement will auto close the connection.
    }

    public void getAllRecords() {
        // Step 1: Establishing a Connection
        try (Connection connection = DriverManager
                .getConnection(getUrl(), getUser(), getPass());

             // Step 2:Create a statement using connection object
             Statement stmt = connection.createStatement();) {

            // Step 3:Execute the statement
            ResultSet rs = stmt.executeQuery("SELECT * FROM public.user;");

            // Step 4: Process the ResultSet object.
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstName = rs.getString("first");
                String lastName = rs.getString("second");

                System.out.println(id + " - " + firstName + " " + lastName);

                System.out.println();
            }
        } catch (SQLException e) {
            ErrorHendler.printSQLException(e);
        }
        // Step 4: try-with-resource statement will auto close the connection.
    }
}