package Helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class WriterDB extends ConnectionData{
    public void insertRecord(int id, String firstName, String lastName) throws SQLException {
        final String INSERT_USER = "INSERT INTO public.user"
                +
                "  (id, first, second) VALUES " +
                " (?, ?, ?);";

        // Step 1: Establishing a Connection
        try (Connection connection = DriverManager
                .getConnection(getUrl(), getUser(), getPass());

             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER)) {
            preparedStatement.setInt(1, id);
            preparedStatement.setString(2, firstName);
            preparedStatement.setString(3, lastName);

            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            preparedStatement.executeUpdate();
        } catch (SQLException e) {

            // print SQL exception information
            ErrorHendler.printSQLException(e);
        }

        // Step 4: try-with-resource statement will auto close the connection.
    }
}