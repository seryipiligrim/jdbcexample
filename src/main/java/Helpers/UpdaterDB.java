package Helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UpdaterDB extends ConnectionData{
    public void updateFirstName(String name, int id) {
        final String UPDATE_USER = "UPDATE public.user SET first = ? where id = ?;";

        // Step 1: Establishing a Connection
        try (Connection connection = DriverManager
                .getConnection(getUrl(), getUser(), getPass());

             // Step 2:Create a statement using connection object
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER);) {
            preparedStatement.setString(1, name);
            preparedStatement.setInt(2, id);
            System.out.println(preparedStatement);
            // Step 3: Execute the query or update query
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            ErrorHendler.printSQLException(e);
        }
        // Step 4: try-with-resource statement will auto close the connection.
    }
}