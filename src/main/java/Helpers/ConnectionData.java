package Helpers;

import java.util.Properties;

public class ConnectionData {
    Properties props = RetrieveProperties.readProperties();

    private String url = props.getProperty("db.url");
    private String user = props.getProperty("db.user");
    private String pass = props.getProperty("db.pass");

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }
}