import java.sql.*;

import Helpers.ErrorHendler;
import Helpers.ReaderDB;
import Helpers.RemoverDB;
import Helpers.UpdaterDB;
import Helpers.WriterDB;

public class Main {
    public static void main(String[] argv) {
        WriterDB writerDB = new WriterDB();
        ReaderDB readerDB = new ReaderDB();
        UpdaterDB updaterDB = new UpdaterDB();
        RemoverDB removerDB = new RemoverDB();
        
        /*
        try {
            writerDB.insertRecord(2, "Ann", "Smith");
        } catch (SQLException e) {
            ErrorHendler.printSQLException(e);
            e.printStackTrace();
        }*/

        //readerDB.getOneRecord(2);
        //updaterDB.updateFirstName("Amanda", 2);
        //removerDB.removeRecord(2);
        readerDB.getAllRecords();
    }
}
